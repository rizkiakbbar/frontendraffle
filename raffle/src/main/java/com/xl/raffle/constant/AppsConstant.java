package com.xl.raffle.constant;

public class AppsConstant {

	public static final String STATUS_WINNER = "WINNER";
	public static final String STATUS_ALL = "ALL";
	public static final String STATUS_AVAILABLE = "AVAILABLE";
	public static final String STATUS_CANCEL = "CANCEL";

}
