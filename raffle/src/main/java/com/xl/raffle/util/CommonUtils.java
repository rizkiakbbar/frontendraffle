package com.xl.raffle.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.xl.raffle.constant.AppsConstant;
import com.xl.raffle.soa.object.Subscriber;

public class CommonUtils {

	public static boolean isValidMsisdn(String msisdn, String prefix) {

		String prefixs[] = prefix.split(";");

		String tmp = "";

		/* generate pattern */
		for (int i = 0; i < prefixs.length; i++) {
			tmp = tmp + "0" + prefixs[i].substring(1);
			if ((i + 1) != prefixs.length)
				tmp = tmp + "|";
		}

		String pattern = "^(" + tmp + ")[0-9]{5,9}$";

		return msisdn != null && msisdn.matches(pattern);
	}

	public static String convertMsisdn(String msisdn) {
		return "62" + msisdn.substring(1, msisdn.length());

	}

	public static String convertMsisdn2(String msisdn) {
		return "0" + msisdn.substring(2, msisdn.length());

	}

	public static String convertNik(String nik) {

		String tmp = "";
		for (int i = 0; i < nik.length(); i++) {
			if (i == 6 || i == 7 || i == 8 || i == 9 || i == 10 || i == 11) {

				tmp += "X";
			} else {
				tmp += nik.charAt(i);
			}
		}

		return tmp;

	}

	public static String convertMsisdnX(String nik) {

		String tmp = "";
		for (int i = 0; i < nik.length(); i++) {
			if (i == 6 || i == 7 || i == 8 || i == 9) {

				tmp += "X";
			} else {
				tmp += nik.charAt(i);
			}
		}

		return tmp;

	}

	public static List<Subscriber> convertList(List<Subscriber> list) {

		List<Subscriber> tmp = new ArrayList<Subscriber>();

		for (Subscriber subscriber : list) {
			subscriber.setMsisdn(CommonUtils.convertMsisdnX(subscriber.getMsisdn()));
			tmp.add(subscriber);
		}
		return tmp;
	}

	public static List<Subscriber> sortById(List<Subscriber> list) {
		Collections.sort(list, new Comparator<Subscriber>() {
			public int compare(Subscriber lhs, Subscriber rhs) {
				// -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
				return Long.valueOf(lhs.getId()) > Long.valueOf(rhs.getId()) ? -1
						: (Long.valueOf(lhs.getId()) < Long.valueOf(rhs.getId())) ? 1 : 0;
			}
		});
		return list;
	}

	// public static List<Subscriber> filterByBatchAndStatus(List<Subscriber> list,
	// String batch) {
	//
	// List<Subscriber> tmp = new ArrayList<Subscriber>();
	//
	// for (Subscriber subscriber : list) {
	// if (subscriber.getBatchId().equals(batch) &&
	// !subscriber.getStatus().equals(AppsConstant.STATUS_AVAILABLE)) {
	// subscriber.setMsisdn(subscriber.getMsisdn());
	// tmp.add(subscriber);
	//
	// }
	// }
	// return tmp;
	// }

}
