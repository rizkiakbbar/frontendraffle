package com.xl.raffle.util;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HttpRestCall {

	Logger logger = Logger.getLogger(HttpRestCall.class);

	public String post(HashMap<String, String> payload) throws Exception {

		logger.info("[REQUEST][" + payload.get("endpoint") + "] - " + payload.get("jsonReq"));

		RestTemplate restTemplate = new RestTemplate();

		((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(90000);

		String jsonRes = restTemplate.postForObject(payload.get("endpoint"), payload.get("jsonReq"), String.class);

		logger.info("[RESPONSE] - " + jsonRes);

		return jsonRes;
	}

	public String get(String endpoint) throws Exception {

		logger.info("[REQUEST][" + endpoint + "]");

		RestTemplate restTemplate = new RestTemplate();

		((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(90000);

		String jsonRes = restTemplate.getForObject(endpoint, String.class);

		logger.info("[RESPONSE] - " + jsonRes);

		return jsonRes;
	}

	public String get(String endpoint, HttpHeaders headers) throws Exception {

		logger.info("[REQUEST][" + endpoint + "]");

		RestTemplate restTemplate = new RestTemplate();

		((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(90000);

		HttpEntity<String> entity = new HttpEntity<String>(headers);

		ResponseEntity<String> jsonRes = restTemplate.exchange(endpoint, HttpMethod.GET, entity, String.class);

		logger.info("[RESPONSE] - " + jsonRes);

		return jsonRes.getBody();
	}

	public String post(HashMap<String, String> payload, HttpHeaders headers) throws Exception {

		logger.info("[REQUEST][" + payload.get("endpoint") + "] - " + payload.get("jsonReq"));

		HttpEntity<String> entity = new HttpEntity<String>(payload.get("jsonReq"), headers);

		RestTemplate restTemplate = new RestTemplate();

		((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(90000);

		String jsonRes = restTemplate.postForObject(payload.get("endpoint"), entity, String.class);

		logger.info("[RESPONSE] - " + jsonRes);

		return jsonRes;
	}

}
