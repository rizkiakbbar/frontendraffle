package com.xl.raffle.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.xl.raffle.bean.User;

@Service
public class UserService {

	Logger logger = Logger.getLogger(UserService.class);

	@Value("${user.list}")
	private String userlist;

	public User getUser(String username, String password) {

		String userList[] = userlist.split(",");

		User user = null;

		for (int i = 0; i < userList.length; i++) {
			String userData[] = userList[i].split(";");

			if (userData[0].equalsIgnoreCase(username) & userData[1].equalsIgnoreCase(password))
				user = new User(userData[0], userData[1], userData[2]);
		}

		return user;

	}

}
