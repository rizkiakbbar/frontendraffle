package com.xl.raffle.service;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xl.raffle.soa.object.CountParticipant;
import com.xl.raffle.soa.request.UpdateRequest;
import com.xl.raffle.soa.response.Subscribers;
import com.xl.raffle.soa.response.UpdateResponse;
import com.xl.raffle.util.HttpRestCall;

@Service
public class SOARestService {

	Logger logger = Logger.getLogger(SOARestService.class);

	@Value("${raffle.participant.endpoint}")
	private String raffleparticipant;

	@Value("${get.participant.endpoint}")
	private String getparticipant;
	
	@Value("${raffle.participant.batch.endpoint}")
	private String raffleparticipantBatch;

	@Value("${get.participant.batch.endpoint}")
	private String getparticipantBatch;

	@Value("${raffleupdate.endpoint}")
	private String raffleupdate;
	
	@Value("${count.participant.endpoint}")
	private String countParticipant;

	@Value("${channel}")
	private String channel;

	@Value("${num.row}")
	private Integer numRow;

	@Autowired
	HttpRestCall caller;

	public Subscribers getParticipant() {
		Subscribers participant = new Subscribers();

		try {

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Channel", channel);

			String soaRs = caller.get(raffleparticipant, headers);

			participant = new ObjectMapper().readValue(soaRs, Subscribers.class);

		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.getMessage());
		}

		return participant;
	}
	
	public Subscribers getParticipantBatch() {
		Subscribers participant = new Subscribers();

		try {

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Channel", channel);

			String soaRs = caller.get(raffleparticipantBatch, headers);

			participant = new ObjectMapper().readValue(soaRs, Subscribers.class);

		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.getMessage());
		}

		return participant;
	}
	
	public CountParticipant getCountParticipant() {
		CountParticipant participant = new CountParticipant();

		try {

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Channel", channel);

			String soaRs = caller.get(countParticipant, headers);

			participant = new ObjectMapper().readValue(soaRs, CountParticipant.class);

		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.getMessage());
		}

		return participant;
	}

	public Subscribers getParticipant(String status) {
		Subscribers participant = new Subscribers();

		try {

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Channel", channel);

			UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(getparticipant)
					.queryParam("status", status).queryParam("numrow", numRow);

			String soaRs = caller.get(builder.toUriString(), headers);
			participant = new ObjectMapper().readValue(soaRs, Subscribers.class);
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.getMessage());
		}

		return participant;
	}
	
	public Subscribers getParticipantBatch(String status) {
		Subscribers participant = new Subscribers();

		try {

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Channel", channel);

			UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(getparticipantBatch)
					.queryParam("status", status).queryParam("numrow", numRow);

			String soaRs = caller.get(builder.toUriString(), headers);
			participant = new ObjectMapper().readValue(soaRs, Subscribers.class);
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.getMessage());
		}

		return participant;
	}

	public UpdateResponse raffleupdate(UpdateRequest request) {
		UpdateResponse res = new UpdateResponse();

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Channel", channel);

			HashMap<String, String> payload = new HashMap<String, String>();

			String jsonReq = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(request);

			payload.put("jsonReq", jsonReq);

			payload.put("endpoint", raffleupdate);

			String soaRs = caller.post(payload, headers);

			res = new ObjectMapper().readValue(soaRs, UpdateResponse.class);

		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.getMessage());
		}
		return res;
	}

}
