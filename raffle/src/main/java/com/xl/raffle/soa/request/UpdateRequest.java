package com.xl.raffle.soa.request;

public class UpdateRequest {

	private String id;

	private String status;

	private String note;

	public UpdateRequest() {
		super();
	}

	public UpdateRequest(String id, String status, String note) {
		super();
		this.id = id;
		this.status = status;
		this.note = note;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
