package com.xl.raffle.soa.object;

public class CountParticipant {
	
	Long totalParticipant;

	public Long getTotalParticipant() {
		return totalParticipant;
	}

	public void setTotalParticipant(Long totalParticipant) {
		this.totalParticipant = totalParticipant;
	}
}
