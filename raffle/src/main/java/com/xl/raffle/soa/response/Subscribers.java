package com.xl.raffle.soa.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.xl.raffle.soa.object.Subscriber;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class Subscribers {

	List<Subscriber> subscribers;

	public List<Subscriber> getSubscribers() {
		return subscribers;
	}

	public void setSubscribers(List<Subscriber> subscribers) {
		this.subscribers = subscribers;
	}

}
