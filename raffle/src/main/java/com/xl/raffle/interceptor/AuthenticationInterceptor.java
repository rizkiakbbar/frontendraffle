package com.xl.raffle.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.xl.raffle.bean.User;

public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

	Logger logger = Logger.getLogger(AuthenticationInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		User user = (User) request.getSession().getAttribute("user");

		if (user == null) {

			response.sendRedirect(request.getContextPath() + "/login");

			return false;
		} else {
			return true;
		}

	}

}