package com.xl.raffle.bean;

import java.util.List;

import com.xl.raffle.soa.object.Subscriber;

public class AjaxBean {

	private List<Subscriber> list;
	private int stock;

	public List<Subscriber> getList() {
		return list;
	}

	public void setList(List<Subscriber> list) {
		this.list = list;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

}
