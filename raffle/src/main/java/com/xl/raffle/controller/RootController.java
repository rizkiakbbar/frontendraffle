package com.xl.raffle.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xl.raffle.bean.LoginBean;
import com.xl.raffle.bean.User;
import com.xl.raffle.constant.AppsConstant;
import com.xl.raffle.security.service.CSRFTokenService;
import com.xl.raffle.service.SOARestService;
import com.xl.raffle.soa.object.Subscriber;
import com.xl.raffle.util.CommonUtils;

@Controller
public class RootController {

	Logger logger = Logger.getLogger(RootController.class);

	@Autowired
	CSRFTokenService csrfTokenService;

	@Autowired
	SOARestService soaRestService;

	@Value("${stock.motor}")
	private Integer stock;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();

		try {
			User user = (User) request.getSession().getAttribute("user");
			model.addObject("csrf", csrfTokenService.getTokenFromSession(request));
			if (user != null) {

				if ("USER".equals(user.getRole())) {
					model.addObject("totalPelanggan", soaRestService.getCountParticipant().getTotalParticipant());
					model.setViewName("page-user-index");
				} else if ("CS".equals(user.getRole())) {
					List<Subscriber> list1 = null;
					List<Subscriber> tmp = soaRestService.getParticipantBatch(AppsConstant.STATUS_AVAILABLE)
							.getSubscribers();
					List<Subscriber> tmp1 = soaRestService.getParticipantBatch(AppsConstant.STATUS_WINNER)
							.getSubscribers();
					List<Subscriber> tmp2 = soaRestService.getParticipantBatch(AppsConstant.STATUS_CANCEL)
							.getSubscribers();
					tmp.addAll(tmp1);tmp.addAll(tmp2);
					// if (tmp != null) {
					// list1 = CommonUtils.filterByBatchAndStatus(
					// soaRestService.getParticipant(AppsConstant.STATUS_ALL).getSubscribers(),
					// tmp.get(0).getBatchId());
					// list1.addAll(tmp);
					// tmp = list1;
					// }

					model.addObject("list", CommonUtils.sortById(tmp));

					model.addObject("winner", AppsConstant.STATUS_WINNER);
					model.addObject("cancel", AppsConstant.STATUS_CANCEL);
					model.addObject("available", AppsConstant.STATUS_AVAILABLE);
					model.setViewName("page-cs-index");
				}
			} else {

				model.addObject("loginBean", new LoginBean());
				model.setViewName("login");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			return new ModelAndView("500");
		}

		return model;

	}

	@RequestMapping(value = "/kandidat-pemenang", method = RequestMethod.GET)
	public ModelAndView kandidatPemenang(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();

		try {
			User user = (User) request.getSession().getAttribute("user");
			model.addObject("csrf", csrfTokenService.getTokenFromSession(request));

			if ("USER".equals(user.getRole())) {
				List<Subscriber> tmp1 = soaRestService.getParticipantBatch(AppsConstant.STATUS_WINNER).getSubscribers();
				List<Subscriber> tmp2 = soaRestService.getParticipantBatch(AppsConstant.STATUS_CANCEL).getSubscribers();
				List<Subscriber> tmp3 = soaRestService.getParticipantBatch(AppsConstant.STATUS_AVAILABLE).getSubscribers();
				List<Subscriber> tmp = new ArrayList<Subscriber>();
				tmp.addAll(tmp1);tmp.addAll(tmp2);tmp.addAll(tmp3);
				List<Subscriber> winners = soaRestService.getParticipant(AppsConstant.STATUS_WINNER).getSubscribers();
				Integer availableStock = stock - winners.size();

				model.addObject("stock1", ((availableStock - 2) <= 0) ? 0 : (availableStock - 2));
				model.addObject("stock2",
						(availableStock >= 2) ? 1 : ((availableStock - 1) <= 0) ? 0 : (availableStock - 1));
				model.addObject("stock3", (availableStock >= 1) ? 1 : 0);
				model.addObject("list", CommonUtils.sortById(tmp));
				model.addObject("winner", AppsConstant.STATUS_WINNER);
				model.addObject("cancel", AppsConstant.STATUS_CANCEL);
				model.addObject("available", AppsConstant.STATUS_AVAILABLE);
				model.setViewName("page-user-kandidat-pemenang");
			} else if ("CS".equals(user.getRole()))
				return new ModelAndView("403");

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			return new ModelAndView("500");
		}

		return model;

	}

	@RequestMapping(value = "/daftar-pemenang", method = RequestMethod.GET)
	public ModelAndView daftarpemenang(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();

		try {
			User user = (User) request.getSession().getAttribute("user");
			model.addObject("csrf", csrfTokenService.getTokenFromSession(request));

			if ("USER".equals(user.getRole())) {

				model.addObject("list", soaRestService.getParticipant(AppsConstant.STATUS_WINNER).getSubscribers());

				model.setViewName("semua-pemenang");

			} else if ("CS".equals(user.getRole()))
				return new ModelAndView("403");

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			return new ModelAndView("500");
		}

		return model;
	}

	@RequestMapping(value = "/history", method = RequestMethod.GET)
	public ModelAndView history(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();

		try {
			User user = (User) request.getSession().getAttribute("user");
			model.addObject("csrf", csrfTokenService.getTokenFromSession(request));

			if ("CS".equals(user.getRole())) {
				List<Subscriber> lWin = soaRestService.getParticipant(AppsConstant.STATUS_WINNER).getSubscribers();
				List<Subscriber> lCan = soaRestService.getParticipant(AppsConstant.STATUS_CANCEL).getSubscribers();
				lWin.addAll(lCan);
				model.addObject("list", lWin);
				model.addObject("winner", AppsConstant.STATUS_WINNER);
				model.addObject("cancel", AppsConstant.STATUS_CANCEL);
				model.addObject("available", AppsConstant.STATUS_AVAILABLE);
				model.setViewName("history");

			} else if ("USER".equals(user.getRole()))
				return new ModelAndView("403");

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			return new ModelAndView("500");
		}

		return model;
	}
}
