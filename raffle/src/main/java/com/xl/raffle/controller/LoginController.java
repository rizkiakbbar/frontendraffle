package com.xl.raffle.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.xl.raffle.bean.LoginBean;
import com.xl.raffle.bean.User;
import com.xl.raffle.security.service.CSRFTokenService;
import com.xl.raffle.service.UserService;

@Controller
public class LoginController {

	Logger logger = Logger.getLogger(LoginController.class);

	@Autowired
	UserService userService;

	@Autowired
	CSRFTokenService csrfTokenService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView();

		try {
			User user = (User) request.getSession().getAttribute("user");
			if (user != null) {
				return new ModelAndView("redirect:/");
			}

			modelAndView.addObject("csrf", csrfTokenService.getTokenFromSession(request));
			modelAndView.addObject("loginBean", new LoginBean());

			modelAndView.setViewName("login");

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			return new ModelAndView("500");
		}

		return modelAndView;
	}

	@RequestMapping(value = "/authentication", method = RequestMethod.POST)

	public ModelAndView authenticate(@ModelAttribute("loginBean") LoginBean bean, HttpServletRequest request,
			RedirectAttributes attr) {

		try {

			User user = userService.getUser(bean.getUsername(), bean.getPassword());

			if (user != null) {
				request.getSession().invalidate();
				request.getSession().setAttribute("user", user);
				return new ModelAndView("redirect:/");

			} else {
				attr.addFlashAttribute("message", "Invalid username or password.");

				return new ModelAndView("redirect:login");
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			return new ModelAndView("500");
		}

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request) {

		request.getSession().invalidate();

		return new ModelAndView("redirect:/");
	}

}
