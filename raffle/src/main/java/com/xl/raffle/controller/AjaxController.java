package com.xl.raffle.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xl.raffle.bean.AjaxBean;
import com.xl.raffle.bean.User;
import com.xl.raffle.constant.AppsConstant;
import com.xl.raffle.service.SOARestService;
import com.xl.raffle.soa.object.Subscriber;
import com.xl.raffle.soa.request.UpdateRequest;
import com.xl.raffle.soa.response.UpdateResponse;
import com.xl.raffle.util.CommonUtils;

@Controller
public class AjaxController {

	Logger logger = Logger.getLogger(AjaxController.class);

	@Autowired
	SOARestService soaRestService;

	@Value("${stock.motor}")
	private Integer stock;

	@ResponseBody
	@RequestMapping(value = "/getraffleparticipant", method = RequestMethod.POST)
	public List<Subscriber> productcatalog(HttpServletRequest request) {
		List<Subscriber> list = new ArrayList<Subscriber>();
		try {
			list = soaRestService.getParticipantBatch().getSubscribers();
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}
		return CommonUtils.convertList(list);
	}

	@ResponseBody
	@RequestMapping(value = "/getraffleparticipantall", method = RequestMethod.POST)
	public AjaxBean getraffleparticipantall(HttpServletRequest request) {

		List<Subscriber> list = new ArrayList<Subscriber>();
		List<Subscriber> winner = new ArrayList<Subscriber>();
		AjaxBean resp = new AjaxBean();
		try {
			List<Subscriber> tmp1 = soaRestService.getParticipantBatch(AppsConstant.STATUS_WINNER).getSubscribers();
			List<Subscriber> tmp2 = soaRestService.getParticipantBatch(AppsConstant.STATUS_CANCEL).getSubscribers();
			List<Subscriber> tmp3 = soaRestService.getParticipantBatch(AppsConstant.STATUS_AVAILABLE).getSubscribers();
			list.addAll(tmp1);list.addAll(tmp2);list.addAll(tmp3);
			
//			list = soaRestService.getParticipant().getSubscribers();
			winner = soaRestService.getParticipant(AppsConstant.STATUS_WINNER).getSubscribers();
			resp.setList(CommonUtils.sortById(list));
			resp.setStock(stock - winner.size());
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}
		return resp;
	}

	@ResponseBody
	@RequestMapping(value = "/raffleupdatewinner", method = RequestMethod.POST)
	public UpdateResponse raffleupdatewinner(HttpServletRequest request) {

		UpdateResponse res = new UpdateResponse();

		try {

			String id = request.getParameter("id");

			User user = (User) request.getSession().getAttribute("user");

			res = soaRestService.raffleupdate(new UpdateRequest(id, AppsConstant.STATUS_WINNER, "OK"));

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.info(e.getMessage());

		}
		return res;
	}

	@ResponseBody
	@RequestMapping(value = "/raffleupdatecancel", method = RequestMethod.POST)
	public UpdateResponse raffleupdatecancel(HttpServletRequest request) {

		UpdateResponse res = new UpdateResponse();

		try {

			String id = request.getParameter("id");

			String note = request.getParameter("note");

			User user = (User) request.getSession().getAttribute("user");

			res = soaRestService
					.raffleupdate(new UpdateRequest(id, AppsConstant.STATUS_CANCEL, note.isEmpty() ? "CANCEL" : note));

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.info(e.getMessage());

		}
		return res;
	}

}
