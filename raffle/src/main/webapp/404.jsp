<%@include file="taglibs.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Ropa+Sans"
	rel="stylesheet">
<style type="text/css">
body {
	font-family: 'Ropa Sans', sans-serif;
	margin-top: 30px;
	background-color: #4C87B9;
	background-color: #4C87B9;
	text-align: center;
	color: #fff;
}

.error-heading {
	margin: 50px auto;
	width: 250px;
	border: 5px solid #fff;
	font-size: 126px;
	line-height: 126px;
	border-radius: 30px;
	text-shadow: 6px 6px 5px #000;
}

.error-heading img {
	width: 100%;
}

.error-main h1 {
	font-size: 72px;
	margin: 0px;
	color: #4C87B9;
	text-shadow: 0px 0px 5px #fff;
}
</style>
</head>
<body>
	<div class="error-main">
		<h1>Oops!</h1>
		<div class="error-heading">404</div>
		<p>
			PAGE NOT FOUND. <a href="${pageContext.request.contextPath}">Back
				to Home</a>
		</p>
	</div>
</body>
</html>
