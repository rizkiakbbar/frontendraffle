<%@include file="taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Surprise Package XL - Acak Pemenang</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Daftar Pemenang Surprise Package XL">

<link
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/resources/assets/css/theme.css"
	rel="stylesheet" type="text/css" media="all" />

<link
	href="${pageContext.request.contextPath}/resources/assets/css/stack-interface.css"
	rel="stylesheet" type="text/css" media="all" />

<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/assets/css/custom.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/resources/assets/css/fonts.css"
	rel="stylesheet" type="text/css" media="all" />

<link rel="icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/assets/img/favicon-32x32.png"
	sizes="32x32">
<link rel="icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/assets/img/favicon-16x16.png"
	sizes="16x16">

</head>
<body class=" ">
	<div class="nav-container">
		<div>
			<nav id="menu1" class="bar bar-1 bar--transparent bar--absolute">
				<div class="container">
					<div class="row">
						<div class="col text-left">
							<div class="bar__module">
								<a href="#"> <img class="logo logo-dark" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
									<img class="logo logo-light" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
								</a>
										</div>
					</div>

			</div>
				</div>
			</nav>
		</div>
	</div>

	<div class="main-container">
		<section class="imagebg height-100 text-center">
			<div class="background-image-holder">
				<img alt="background"
					src="${pageContext.request.contextPath}/resources/assets/img/bg.jpg" />
			</div>

			<div class="container text-center pos-vertical-center">
				<span><h3 class="text-shadow" style="margin-bottom:0px;">UNDIAN BERHADIAH</h3>
					<h1 class="text-shadow" style="font-size: 5em">SURPRISE PACKAGE</h1></span>
				<img
					src="${pageContext.request.contextPath}/resources/assets/img/mix.png">
				<div class="row justify-content-center">
					<div class="col-md-4">

						<div class="modal-instance" style="width: 100%">
							<a class="btn btn--primary-2 btn--lg modal-trigger" href="#"
								id="acak" style="width: 100%"> <span
								class="btn__text color--primary">ACAK</span>
							</a>
							<p>Total kupon ID : ${totalPelanggan} kupon</p>

							<div class="modal-container text-center">
								<div class="modal-content">
									<img
										src="${pageContext.request.contextPath}/resources/assets/img/celebration.svg"><br>
									<a id="stop" class="btn btn--primary-2 btn--lg modal-close"
										href="${pageContext.request.contextPath}/kandidat-pemenang">
										<span class="btn__text color--primary">STOP</span>
									</a>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!--end of container-->
		</section>
	</div>

	<script
		src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.1.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/scripts.js"></script>

	<script>
		$('#stop').hide();
		$('#acak')
				.click(
						function() {
							$
									.ajax(
											{
												method : 'POST',
												url : '${pageContext.request.contextPath}/getraffleparticipant',
												data : {
													_tk : '${csrf}'
												}
											})
									.done(function(msg) {
										$('#stop').show();
									})
									.fail(
											function(xhr, status, error) {
												window.location
														.assign("${pageContext.request.contextPath}");
											});
						});
	</script>
</body>
</html>

<!-- <div class="boxed bg--white box-shadow" style="padding:0">
                            </div> -->