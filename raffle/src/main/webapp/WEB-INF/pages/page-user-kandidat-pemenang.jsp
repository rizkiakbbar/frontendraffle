<%@include file="taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Surprise Package XL - Kandidat Pemenang</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Daftar Pemenang Dapetin XL">

<link
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/resources/assets/css/theme.css"
	rel="stylesheet" type="text/css" media="all" />

<link
	href="${pageContext.request.contextPath}/resources/assets/css/stack-interface.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/assets/css/custom.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/resources/assets/css/fonts.css"
	rel="stylesheet" type="text/css" media="all" />

<link rel="icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/assets/img/favicon-32x32.png"
	sizes="32x32">
<link rel="icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/assets/img/favicon-16x16.png"
	sizes="16x16">
	
</head>
<body class=" ">
	<div class="nav-container">
		<div>
			<nav id="menu1" class="bar bar-1 bar--transparent bar--absolute">
				<div class="container" style="max-width: 98%">
					<div class="row">
						<div class="col-6 text-left">
							<div class="bar__module">
								<a href="#"> <img class="logo logo-dark" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
									<img class="logo logo-light" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
								</a>
								<%-- <a href="${pageContext.request.contextPath}" class="block">
									<div class="feature boxed box-shadow"
										style="padding: 3px; height: 42px; margin-bottom: 0px; width: 42px;">
										<i class="material-icons icon--lg color--primary-1"
											style="font-size: 36px;">home</i>
									</div>
								</a> --%>
							</div>
						</div>
						<div class="col-6 text-right">
							<div class="bar__module">
								<%-- <a href="#"> <img class="logo logo-dark" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
									<img class="logo logo-light" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
								</a> --%>
								<a href="${pageContext.request.contextPath}" class="block">
									<!-- <div class="feature boxed box-shadow"
										style="padding: 3px; height: 42px; margin-bottom: 0px; width: 42px;"> -->
									<i class="material-icons icon--lg color--primary-1"
									style="font-size: 36px;">home</i> <!-- </div> -->
								</a>
							</div>
						</div>
					</div>
				</div>
			</nav>
		</div>
	</div>

	<div class="main-container">
		<section class="imagebg height-100">
			<div class="background-image-holder">
				<img alt="background"
					src="${pageContext.request.contextPath}/resources/assets/img/bg.jpg" />
			</div>
			<div class="container text-center pos-vertical-center">
				<h1 class="text-shadow" style="margin-bottom: 0">Daftar
					Kandidat Pemenang</h1>
				<div class="row justify-content-center">
					<div class="col-md-4">

						<div class="boxed imagebg box-shadow" style="padding: 1em">
							<div class="background-image-holder">
								<img alt="background"
									src="${pageContext.request.contextPath}/resources/assets/img/bg-box.jpg" />
							</div>

							<div class="row justify-content-center">
								<div class="col-md-5">
									<img
										src="${pageContext.request.contextPath}/resources/assets/img/money.png"
										style="height: 80px">
								</div>
								<div class="col-md-5">
									<h1 id="stock1" class="text-shadow"
										style="margin-bottom: 0; font-size: 4em; line-height: 1em">
										<c:out value="${stock1}"></c:out>
									</h1>
									<h5 class="text-shadow" style="margin-bottom: 0">Rp.5.000.000	</h5>

								</div>
							</div>
						</div>


					</div>
					
					<div class="col-md-4">

						<div class="boxed imagebg box-shadow" style="padding: 1em">
							<div class="background-image-holder">
								<img alt="background"
									src="${pageContext.request.contextPath}/resources/assets/img/bg-box.jpg" />
							</div>

							<div class="row justify-content-center">
								<div class="col-md-5">
									<img
										src="${pageContext.request.contextPath}/resources/assets/img/gold.png"
										style="height: 80px">
								</div>
								<div class="col-md-5">
									<h1 id="stock2" class="text-shadow"
										style="margin-bottom: 0; font-size: 4em; line-height: 1em">
										<c:out value="${stock2}"></c:out>
									</h1>
									<h5 class="text-shadow" style="margin-bottom: 0">Emas batang 100 gram</h5>

								</div>
							</div>
						</div>


					</div>
					
					<div class="col-md-4">

						<div class="boxed imagebg box-shadow" style="padding: 1em">
							<div class="background-image-holder">
								<img alt="background"
									src="${pageContext.request.contextPath}/resources/assets/img/bg-box.jpg" />
							</div>

							<div class="row justify-content-center">
								<div class="col-md-5">
									<img
										src="${pageContext.request.contextPath}/resources/assets/img/motor2.png"
										style="height: 80px">
								</div>
								<div class="col-md-5">
									<h1 id="stock3" class="text-shadow"
										style="margin-bottom: 0; font-size: 4em; line-height: 1em">
										<c:out value="${stock3}"></c:out>
									</h1>
									<h5 class="text-shadow" style="margin-bottom: 0">Motor Genio</h5>

								</div>
							</div>
						</div>


					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-10 text-right">
						<table
							class="text-center bg--white border--round table--alternate-row">
							<thead>
								<tr>
									<th>No.</th>
									<th>MSISDN</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody id="bodyTable">
								<c:forEach items="${list}" var="row">
									<c:set var="count" value="${count + 1}" scope="page" />
									<tr>
										<td><c:out value="${count}"></c:out></td>
										<td><c:out value="${row.msisdn}"></c:out></td>
										<c:if test="${row.status == available}">
											<td><i class="icon material-icons icon--xs"
												style="color: #252525">do_not_disturb_on</i></td>
										</c:if>
										<c:if test="${fn:contains(row.status, 'WINNER')}">
											<td><i
												class="icon material-icons icon--xs color--primary-1">check_circle</i></td>
										</c:if>
										<c:if test="${row.status == cancel}">
											<td><i class="material-icons icon--xs"
												style="color: red">cancel</i></td>
										</c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<div class="row" style="margin-top: 10px">
							<div class="col-6 text-left">
								<span> <i
									class="icon material-icons icon--xs color--primary-1">check_circle</i>
									= Diterima <i class="material-icons icon--xs"
									style="color: red">cancel</i> = Ditolak <i
									class="icon material-icons icon--xs" style="color: #252525">do_not_disturb_on</i>
									= Menunggu Konfirmasi

								</span>
							</div>

							<div class="col-6">
								<a href="${pageContext.request.contextPath}/daftar-pemenang"
									target="_blank">Lihat Semua Pemenang >></a>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!--end of container-->
		</section>
	</div>

	<script
		src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.1.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/scripts.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/moment.js"></script>

	<script type="text/javascript">
		var batch = '';
		setInterval(
				function() {
					$
							.ajax(
									{
										method : 'POST',
										url : '${pageContext.request.contextPath}/getraffleparticipantall',
										data : {
											_tk : '${csrf}',
											batch : batch
										}
									})
							.done(
									function(resp) {
										var msg = resp.list;
										var str = '<tbody id="bodyTable">';
										for (let i = 0; i < msg.length; i++) {
											str = str
													+ '<tr><td>'
													+ (i + 1)
													+ '</td><td>'
													+ msg[i].msisdn
													+ '</td>';
											if (msg[i].status.includes('${winner}'))
												str = str
														+ '<td><i class="icon material-icons icon--xs color--primary-1">check_circle</i></td>';
											if (msg[i].status == '${available}')
												str = str
														+ '<td><i class="icon material-icons icon--xs" style="color: #252525">do_not_disturb_on</i></td>';
											if (msg[i].status == '${cancel}')
												str = str
														+ '<td><i class="material-icons icon--xs" style="color: red">cancel</i></td>';
											batch = msg[i].batch_id;
										}
										str = str + '</tbody>';

										if (msg.length > 0) {
											var stock1 = ((resp.stock - 2)<=0)?0:(resp.stock - 2);
											var stock2 = (resp.stock >= 2)?1:((resp.stock - 1)<=0)?0:(resp.stock - 1);
											var stock3 = (resp.stock >= 1)?1:0;
											console.log(stock1);
											$("#bodyTable").replaceWith(str);
											$("#stock1")
													.replaceWith(
															'<h1 id="stock1" class="text-shadow" style="margin-bottom: 0; font-size: 4em; line-height: 1em">'
																	+ stock1
																	+ '</h1>');
											$("#stock2")
											.replaceWith(
													'<h1 id="stock2" class="text-shadow" style="margin-bottom: 0; font-size: 4em; line-height: 1em">'
															+ stock2
															+ '</h1>');
											
											$("#stock3")
											.replaceWith(
													'<h1 id="stock3" class="text-shadow" style="margin-bottom: 0; font-size: 4em; line-height: 1em">'
															+ stock3
															+ '</h1>');
										}

									})
							.fail(
									function(xhr, status, error) {
										window.location
												.assign("${pageContext.request.contextPath}");
									});

				}, 6000);
	</script>
</body>
</html>

<!-- <div class="boxed bg--white box-shadow" style="padding:0">
                            </div> -->