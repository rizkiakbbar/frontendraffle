<%@include file="taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Surprise Package XL - History Pemenang</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Daftar Pemenang Surprise Package XL">

<link
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/resources/assets/css/theme.css"
	rel="stylesheet" type="text/css" media="all" />

<link
	href="${pageContext.request.contextPath}/resources/assets/css/stack-interface.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/assets/css/custom.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/resources/assets/css/fonts.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="http://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" media="all" />




<%-- <link
	href="${pageContext.request.contextPath}/resources/assets/plugins/DataTables-1.10.16/css/dataTables.bootstrap.min.css"
	rel="stylesheet" type="text/css" media="all" /> --%>
<%-- <link
	href="${pageContext.request.contextPath}/resources/assets/plugins/DataTables-1.10.16/datatables.min.css"
	rel="stylesheet" type="text/css" media="all" />

<link
	href="${pageContext.request.contextPath}/resources/assets/plugins/DataTables-1.10.16/datatables.min.css"
	rel="stylesheet" type="text/css" media="all" /> --%>





<link rel="icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/assets/img/favicon-32x32.png"
	sizes="32x32">
<link rel="icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/assets/img/favicon-16x16.png"
	sizes="16x16">

</head>
<body class=" ">
	<div class="nav-container">
		<div>
			<nav id="menu1" class="bar bar-1 bar--transparent bar--absolute">
				<%-- 				<div class="container" style="max-width: 98%">
					<div class="row">
						<div class="col-6 text-left">
							<div class="bar__module">
								<a href="${pageContext.request.contextPath}" class="block">
									<div class="feature boxed box-shadow"
										style="padding: 3px; height: 42px; margin-bottom: 0px; width: 42px;">
										<i class="material-icons icon--lg color--primary-1"
											style="font-size: 36px;">home</i>
									</div>
								</a>
							</div>
						</div>
						<div class="col-6 text-right">
							<div class="bar__module">
								<a href="#"> <img class="logo logo-dark" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
									<img class="logo logo-light" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
								</a>
							</div>
						</div>
					</div>
				</div> --%>
				<div class="container" style="max-width: 98%">
					<div class="row">
						<div class="col-6 text-left">
							<div class="bar__module">
								<a href="${pageContext.request.contextPath}"> <img
									class="logo logo-dark" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
									<img class="logo logo-light" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
								</a>
								<%-- <a href="${pageContext.request.contextPath}" class="block">
									<div class="feature boxed box-shadow"
										style="padding: 3px; height: 42px; margin-bottom: 0px; width: 42px;">
										<i class="material-icons icon--lg color--primary-1"
											style="font-size: 36px;">home</i>
									</div>
								</a> --%>
							</div>
						</div>
						<div class="col-6 text-right">
							<div class="bar__module">
								<%-- <a href="#"> <img class="logo logo-dark" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
									<img class="logo logo-light" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
								</a> --%>
								<a href="${pageContext.request.contextPath}" class="block">
									<!-- <div class="feature boxed box-shadow"
										style="padding: 3px; height: 42px; margin-bottom: 0px; width: 42px;"> -->
									<i class="material-icons icon--lg color--primary-1"
									style="font-size: 36px;">home</i> <!-- </div> -->
								</a>
							</div>
						</div>
					</div>
				</div>
			</nav>
		</div>
	</div>
	<div class="main-container">
		<section class="imagebg height-100">
			<div class="background-image-holder" style="position: fixed">
				<img alt="background"
					src="${pageContext.request.contextPath}/resources/assets/img/bg.jpg" />
			</div>
			<div class="container text-center" style="margin-bottom: 80px">
				<h1 class="text-shadow">History Pemenang Surprise Package XL</h1>
				<div class="row justify-content-center">
					<div class="col-md-12 text-right">
						<table
							class="text-center bg--white border--round table--alternate-row"
							id="myTable">
							<thead>
								<tr>
									<th>No.</th>
									<th>MSISDN</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody id="bodyTable">
								<c:forEach items="${list}" var="row">
									<c:set var="count" value="${count + 1}" scope="page" />
									<tr>
										<td><c:out value="${count}"></c:out></td>
										<td><c:out value="${row.msisdn}"></c:out></td>
										<c:if test="${row.status == available}">
											<td><i class="icon material-icons icon--xs"
												style="color: #252525">do_not_disturb_on</i></td>
										</c:if>
										<c:if test="${fn:contains(row.status, 'WINNER') }">
											<td><i
												class="icon material-icons icon--xs color--primary-1">check_circle</i></td>
										</c:if>
										<c:if test="${row.status == cancel}">
											<td><i class="material-icons icon--xs"
												style="color: red">cancel</i></td>
										</c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<div class="row" style="margin-top: 10px">
							<div class="col-6 text-left">
								<span> <i
									class="icon material-icons icon--xs color--primary-1">check_circle</i>
									= Terima <i class="material-icons icon--xs" style="color: red">cancel</i>
									= Tolak <i class="icon material-icons icon--xs"
									style="color: #252525">do_not_disturb_on</i> = Menunggu
									Konfirmasi

								</span>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!--end of container-->
		</section>
	</div>

	<script
		src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.1.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/scripts.js"></script>
	<%-- <script
		src="${pageContext.request.contextPath}/resources/assets/plugins/DataTables-1.10.16/js/dataTables.bootstrap.min.js"></script> --%>
	<script
		src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

	<script>
		$(document).ready(function() {
			$('#myTable').DataTable({
				"ordering" : false
			});
		});
	</script>
</body>
</html>

<!-- <div class="boxed bg--white box-shadow" style="padding:0">
                            </div> -->