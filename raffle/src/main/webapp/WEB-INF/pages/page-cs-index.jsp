<%@include file="taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Surprise Package XL - Daftar Kandidat Pemenang</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Daftar Pemenang Surprise Package XL">

<link
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/resources/assets/css/theme.css"
	rel="stylesheet" type="text/css" media="all" />

<link
	href="${pageContext.request.contextPath}/resources/assets/css/stack-interface.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/assets/css/custom.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/resources/assets/css/fonts.css"
	rel="stylesheet" type="text/css" media="all" />

<%-- <link
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" media="all" /> --%>

<link rel="icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/assets/img/favicon-32x32.png"
	sizes="32x32">
<link rel="icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/assets/img/favicon-16x16.png"
	sizes="16x16">

</head>
<body class=" ">
	<div class="nav-container">
		<div>
			<nav id="menu1" class="bar bar-1 bar--transparent bar--absolute">
				<div class="container">
					<div class="row">
						<div class="col text-left">
							<div class="bar__module">
								<a href="${pageContext.request.contextPath}"> <img
									class="logo logo-dark" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
									<img class="logo logo-light" alt="logo"
									src="${pageContext.request.contextPath}/resources/assets/img/logo-xl.svg">
								</a>
							</div>
						</div>
					</div>
				</div>
			</nav>
		</div>
	</div>

	<div class="main-container">
		<section class="imagebg height-100">
			<div class="background-image-holder">
				<img alt="background"
					src="${pageContext.request.contextPath}/resources/assets/img/bg.jpg" />
			</div>
			<div class="container text-center pos-vertical-center">
				<h1 class="text-shadow" style="margin-bottom: 0">Daftar
					Kandidat Pemenang</h1>
				<h3>Surprise Package XL</h3>
				<div class="row justify-content-center">
					<div class="col-md-12 text-right">
						<table
							class="text-center bg--white border--round table--alternate-row">
							<thead>
								<tr>
									<th>No.</th>
									<th>MSISDN</th>
									<th>Status</th>
									<th></th>
								</tr>
							</thead>
							<tbody id="bodyTable">
								<c:forEach items="${list}" var="row">
									<c:set var="count" value="${count + 1}" scope="page" />
									<tr>
										<td><c:out value="${count}"></c:out></td>
										<td><c:out value="${row.msisdn}"></c:out></td>
										<c:if test="${row.status == available}">
											<td><i class="icon material-icons icon--xs"
												style="color: #252525">do_not_disturb_on</i></td>
										</c:if>
										<c:if test="${fn:contains(row.status, 'WINNER')}">
											<td><i
												class="icon material-icons icon--xs color--primary-1">check_circle</i></td>
										</c:if>
										<c:if test="${row.status == cancel}">
											<td><i class="material-icons icon--xs"
												style="color: red">cancel</i></td>
										</c:if>

										<td><c:if test="${row.status == available}">
												<a onclick="terima(${row.id})"
													class="btn btn--xs btn--primary-1" href="#"> <span
													class="btn__text" style="color: #fff">Terima</span>
												</a>
												<a onclick="tolak(${row.id})"
													class="btn btn--xs btn--primary-2" href="#"
													style="margin-left: 0"> <span
													class="btn__text color--primary">Tolak</span>
												</a>
											</c:if></td>

									</tr>
								</c:forEach>
							</tbody>
						</table>


						<div class="row" style="margin-top: 20px">
							<div class="col-6 text-left">
								<span> <i
									class="icon material-icons icon--xs color--primary-1">check_circle</i>
									= Diterima <i class="material-icons icon--xs"
									style="color: red">cancel</i> = Ditolak <i
									class="icon material-icons icon--xs" style="color: #252525">do_not_disturb_on</i>
									= Menunggu Konfirmasi

								</span>
							</div>

							<div class="col-6">
								<a class="btn btn--primary-2"
									href="${pageContext.request.contextPath}"> <span
									class="btn__text color--primary">REFRESH</span>
								</a> <a target="_blank" class="btn btn--primary-2"
									href="${pageContext.request.contextPath}/history"> <span
									class="btn__text color--primary">History</span>
								</a>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!--end of container-->
		</section>
	</div>

	<script
		src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.1.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/scripts.js"></script>
	<%-- <script
		src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script> --%>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/sweetalert.min.js"></script>


	<script>
	function terima(id){
		swal({
			  title: "Are you sure?",
			  text: "",
			  icon: "success",
			  buttons: true,
			  dangerMode: true,
			})
			.then((willDelete) => {
			  if (willDelete) { 
				  $
					.ajax(
							{
								method : 'POST',
								url : '${pageContext.request.contextPath}/raffleupdatewinner',
								data : {
									_tk : '${csrf}',
									id : id
								}
							})
					.done(
							function(msg) {
								window.location.assign("${pageContext.request.contextPath}");
							})
					.fail(
							function(xhr, status, error) {
								
							});
			  } else {
			  
			  }
			});
	}
	function tolak(id){
		swal("Note :", {
			content: "input",
			})
			.then((note) => {
				if (note) { 
					$
					.ajax(
							{
								method : 'POST',
								url : '${pageContext.request.contextPath}/raffleupdatecancel',
								data : {
									_tk : '${csrf}',
									id : id,
									note: note
								}
							})
					.done(
							function(msg) {
								window.location.assign("${pageContext.request.contextPath}");
							})
					.fail(
							function(xhr, status, error) {
								
							});
				}				 
			});
	}	
	</script>
</body>
</html>

<!-- <div class="boxed bg--white box-shadow" style="padding:0">
                            </div> -->