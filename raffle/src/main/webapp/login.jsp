<%@include file="taglibs.jsp"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Surprise Package XL - Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Daftar Pemenang Surprise Package XL">

<link
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/resources/assets/css/theme.css"
	rel="stylesheet" type="text/css" media="all" />

<link
	href="${pageContext.request.contextPath}/resources/assets/css/stack-interface.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/assets/css/custom.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/resources/assets/css/fonts.css"
	rel="stylesheet" type="text/css" media="all" />

<link rel="icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/assets/img/favicon-32x32.png"
	sizes="32x32">
<link rel="icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/assets/img/favicon-16x16.png"
	sizes="16x16">

</head>
<body class=" ">
	<div class="main-container">
		<section class="imagebg height-100">
			<div class="background-image-holder">
				<img alt="background"
					src="${pageContext.request.contextPath}/resources/assets/img/bg.jpg" />
			</div>
			<div class="container text-center pos-vertical-center">
				<h1 class="text-shadow">Surprise Package XL</h1>
				<div class="row justify-content-center">
					<div class="boxed boxed--border">
						<c:if test="${not empty message}">
							<i style="color: red">${message}</i>
						</c:if>
						<form:form method="POST"
							action="${pageContext.request.contextPath}/authentication"
							commandName="loginBean">
							<input name="_tk" value="${csrf}" class="hidden">
							<div class="col-md-12">
								<span>Username:</span>
								<form:input type="text" path="username" required="true" />

							</div>
							<div class="col-md-12">
								<span>Password:</span>
								<form:input type="password" path="password" required="true" />
							</div>
							<div class="col-md-12 boxed">
								<button type="submit" class="btn btn--primary type--uppercase">Submit</button>
							</div>
						</form:form>
					</div>
				</div>
			</div>
			<!--end of container-->
		</section>
	</div>

	<script
		src="${pageContext.request.contextPath}/resources/assets/js/jquery-3.1.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/assets/js/scripts.js"></script>
</body>
</html>

<!-- <div class="boxed bg--white box-shadow" style="padding:0">
                            </div> -->